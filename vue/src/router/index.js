import { createRouter, createWebHistory } from 'vue-router'

const routes = [{
        path: '/menu',
        name: 'menu.menu',
        component: () =>
            import ("../views/menu/Menu.vue")
    },
    {
        path: '/menu-create',
        name: 'menu.create',
        component: () =>
            import ("../views/menu/Create.vue")
    },
    {
        path: '/menu-edit/:id',
        name: 'menu.edit',
        component: () =>
            import ("../views/menu/Edit.vue")
    },
    {
        path: '/',
        name: 'home',
        component: () =>
            import ("../views/Home.vue")
    },
    {
        path: '/reservasi',
        name: 'reservasi',
        component: () =>
            import ("../views/Reservasi.vue")
    },
    {
        path: '/detail-reservasi',
        name: 'detail reservasi',
        component: () =>
            import ("../views/Detail-Reservasi.vue")
    },
    {
        path: '/reservasi-sukses',
        name: 'reservasi sukses',
        component: () =>
            import ("../views/Reservasi-Sukses.vue")
    },
    {
        path: '/login',
        name: 'login',
        component: () =>
            import ("../views/Login.vue")
    },
    {
        path: '/register',
        name: 'register',
        component: () =>
            import ("../views/Register.vue")
    },
    {
        path: '/register',
        name: 'register',
        component: () =>
            import ("../views/Register.vue")
    },
    {
        path: '/menus/:id',
        name: 'single-menus',
        component: () =>
            import ("../views/SingleMenu.vue")
    },
    {
        path: '/confirmation',
        name: 'confirmation',
        component: () =>
            import ("../views/Confirmation.vue")
    },
    {
        path: '/checkout/:id',
        name: 'checkout',
        component: () =>
            import ("../views/Checkout.vue"),
        // props: (route) => ({ mid: route.query.mid })
    },
    {
        path: '/dashboard',
        name: 'userboard',
        component: () =>
            import ("../views/UserBoard.vue"),
        meta: { requireAuth: true, is_user: true }
    },
    {
        path: '/admin/:page',
        name: 'admin-pages',
        component: () =>
            import ("../views/Admin.vue"),
        meta: { requireAuth: true, is_admin: true }
    },
    {
        path: '/admin',
        name: 'admin',
        component: () =>
            import ("../views/Admin.vue"),
        meta: { requireAuth: true, is_admin: true }
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes
});



router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (localStorage.getItem('bequeenToken.jwt') == null) {
            next({
                path: '/login',
                params: { nextUrl: to.fullPath }
            })
        } else {
            let user = JSON.parse(localStorage.getItem('bequeenToken.user'))
            if (to.matched.some(record => record.meta.is_admin)) {
                if (user.is_admin == 1) {
                    next()
                } else {
                    next({ name: 'userboard' })
                }
            } else if (to.matched.some(record => record.meta.is_user)) {
                if (user.is_admin == 0) {
                    next()
                } else {
                    next({ name: 'admin' })
                }
            }
            next()
        }
    } else {
        next()
    }
})

export default router;