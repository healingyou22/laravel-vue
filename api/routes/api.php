<?php

use App\Http\Controllers\MenuController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ReservationController;
use App\Http\Controllers\ReservController;
use App\Models\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::get('/menu', [MenuController::class, 'index']);
// Route::get('/menu/{id}', [MenuController::class, 'show']);
// Route::post('/menu', [MenuController::class, 'store']);
// Route::put('/menu/{id}', [MenuController::class, 'update']);
// Route::delete('/menu/{id}', [MenuController::class, 'destroy']);

// Route::resource('/menu', MenuController::class)->except(['create', 'edit']);
Route::post('/login', [UserController::class, 'login']);
Route::post('/register', [UserController::class, 'register']);
Route::get('/menus', [MenuController::class, 'index']);
Route::get('/menuz', [MenuController::class, 'getMenuForHome']);
Route::get('/menus/{menu}', [MenuController::class, 'show']);


Route::group(['middleware' => 'auth:api'], function () {
    Route::get('/users', [UserController::class, 'index']);
    Route::get('/users/{user}', [UserController::class, 'show']);
    Route::patch('/users/{user}', [UserController::class, 'update']);
    Route::get('/users/{user}/orders', [UserController::class, 'showOrders']);
    Route::resource('/orders', OrderController::class);
    Route::resource('/menus', MenuController::class)->except(['index', 'show']);
    Route::post('/reserv', [ReservController::class, 'store']);
    Route::get('/reserv', [ReservController::class, 'index']);
});
