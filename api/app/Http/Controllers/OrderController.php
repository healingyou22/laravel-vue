<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Reserv;
use Auth;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index()
    {
        // return response()->json(Order::with(['menu'])->get(), 200);
        return response()->json(Order::with(['user', 'menu'])->get(), 200);
    }

    public function store(Request $request)
    {
        if (isset($request->quantity) != 0) {
            $reserv = Reserv::where('user_id', Auth::id())->first();
            $harga = $request->price * $request->quantity;

            $reserv->total_price += $harga;
            $reserv->update();

            $order = Order::create([
                'menu_id' => $request->menu_id,
                'user_id' => Auth::id(),
                'quantity' => $request->quantity,
                'price' => $harga,
                'reserv_id' => $reserv->id,
            ]);

            return response()->json([
                'status' => (bool) $order,
                'data'   => $order,
                'message' => $order ? 'Order Created!' : 'Error Creating Order'
            ]);
        }
    }

    public function show(Order $order)
    {
        return response()->json($order, 200);
    }

    public function update(Request $request, Order $order)
    {
        $status = $order->update(
            $request->only(['quantity'])
        );

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Order Updated!' : 'Error Updating Order'
        ]);
    }

    public function destroy(Order $order)
    {
        $status = $order->delete();

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Order Deleted!' : 'Error Deleting Order'
        ]);
    }
}
