<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    public function index()
    {
        return response()->json(Menu::all(), 200);
    }

    public function getMenuForHome()
    {
        return response()->json(Menu::take(6)->get(), 200);
    }

    public function store(Request $request)
    {
        $menu = Menu::create([
            'menu_name' => $request->menu_name,
            'menu_price' => $request->menu_price,
            'menu_image' => $request->menu_image,
            'menu_desc' => $request->menu_desc,
        ]);

        return response()->json([
            'status' => (bool) $menu,
            'data'   => $menu,
            'message' => $menu ? 'Menu Created!' : 'Error Creating Menu'
        ]);
    }

    public function show(Menu $menu)
    {
        return response()->json($menu, 200);
    }

    public function uploadFile(Request $request)
    {
        if ($request->hasFile('menu_image')) {
            $name = time() . "_" . $request->file('menu_image')->getClientOriginalName();
            $request->file('menu_image')->move(public_path('images'), $name);
        }
        return response()->json(asset("images/$name"), 201);
    }

    public function update(Request $request, Menu $menu)
    {
        $status = $menu->update(
            $request->only(['menu_name', 'menu_price', 'menu_image', 'menu_desc'])
        );

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Menu Updated!' : 'Error Updating Menu'
        ]);
    }

    public function destroy(Menu $menu)
    {
        $status = $menu->delete();

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Menu Deleted!' : 'Error Deleting Menu'
        ]);
    }
}
