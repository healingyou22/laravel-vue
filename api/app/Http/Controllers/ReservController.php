<?php

namespace App\Http\Controllers;

use App\Models\Reserv;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth as FacadesAuth;

class ReservController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Reserv::where('user_id', Auth::id())->with(['user'])->first(), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        if ($user->status == 1) {
            $reserv = Reserv::create([
                'PhoneNum' => $request->PhoneNum,
                'user_id' => Auth::id(),
                'GuestNum' => $request->GuestNum,
                'date' => $request->date,
                'time' => $request->time,
                'note' => $request->note,
                'status' => 1
            ]);

            $user->status = 2;
            $user->save();
            return response()->json([
                'status' => (bool) $reserv,
                'data'   => $reserv,
                'message' => $reserv ? 'Reservation Created!' : 'Error Creating Reservation'
            ]);
        };
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Reserv  $reserv
     * @return \Illuminate\Http\Response
     */
    public function show(Reserv $reserv)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Reserv  $reserv
     * @return \Illuminate\Http\Response
     */
    public function edit(Reserv $reserv)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Reserv  $reserv
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reserv $reserv)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Reserv  $reserv
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reserv $reserv)
    {
        //
    }
}
