<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Menu extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'menu_name', 'menu_price', 'menu_image', 'menu_desc'
    ];

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
