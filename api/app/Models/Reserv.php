<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Reserv extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'PhoneNum',
        'GuestNum',
        'date',
        'time',
        'note',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function order()
    {
        return $this->hasMany(Order::class);
    }
}
