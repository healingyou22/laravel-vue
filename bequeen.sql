-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 24, 2021 at 05:08 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bequeen`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `menu_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `menu_price` double NOT NULL,
  `menu_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `menu_desc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notes` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `menu_name`, `menu_price`, `menu_image`, `menu_desc`, `notes`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Steak Marinade', 50000, 'makanan1.jpg', 'It’s deliciously robust and the perfect flavor pairing to rich beefy steaks, and delicious taste!', '-', NULL, NULL, NULL),
(2, 'Honolulu Ice Cream', 55000, 'makanan2.jpg', 'Who doesnt like great ice cream with pineapple chunks. People loved it! yummy and tasty gelato', '-', NULL, NULL, NULL),
(3, 'Fish Tacos', 60000, 'makanan3.jpg', 'Yum! Try the frest fish and chips. This Fish Tacos that melt in your mouth. Done perfectly!', '-', NULL, NULL, NULL),
(4, 'Biscoff Cheesecake', 65000, 'makanan4.jpg', 'No Bake Mini Lotus Biscoff Cheesecake which has a sweet, soft, and crunchy taste. You must try this!', '-', NULL, NULL, NULL),
(5, 'Butter Trout', 50000, 'makanan5.jpg', 'Italian herb seasoning, chopped fresh parsley and garlic give great flavor to the fish! So Healthy!', '-', NULL, NULL, NULL),
(6, 'Classic Pizza', 55000, 'makanan6.jpg', 'The best pizza recipe made with homemade crust, quick tomato sauce, and just the right amount of cheese', '-', NULL, NULL, NULL),
(7, 'Smoothie Bowls', 60000, 'makanan7.jpg', 'A creamy smoothie, you can eat it with a spoon. Top with fresh fruit, granola, or any other healthy toppings', '-', NULL, NULL, NULL),
(8, 'Butternut puree', 65000, 'makanan8.jpg', 'The toasted hazelnuts with crunchy touch and the poached egg and mash yolk, is just magical!', '-', NULL, NULL, NULL),
(9, 'Cocktail Lemon-berry', 20000, 'minuman1.jpg', 'This cocktail full of fresh lemon, sweet strawberry, and beautiful ripe strawberries. Delightful and refreshing!', '-', NULL, NULL, NULL),
(10, 'Monkeypod Mai Tai', 25000, 'minuman2.jpg', 'This Hawaiian Mai Tai Monkeypod is one of the most refreshing and delicious cocktails. With some citrus lemon and orange zest', '-', NULL, NULL, NULL),
(11, 'Twisted Mermaid', 30000, 'minuman3.jpg', 'Jump off the deep and go crazy with our Twisted Mermaid! Made with Blue Curacao, Orange juice and Passion Fruit Juice!', '-', NULL, NULL, NULL),
(12, 'Pyrat Rum Punch', 35000, 'minuman4.jpg', 'The cocktail, served in a hollow pineapple, is a combination of Pyrat Pistol rum, Galliano, apricot brandy, and tropical juices', '-', NULL, NULL, NULL),
(13, 'Dalgona Hot Choco', 20000, 'minuman5.jpg', 'Dalgona Whipped Chocolate is the caffeine-free version of the trendy Dalgona Coffee and is a perfect treat for the kids', '-', NULL, NULL, NULL),
(14, 'Coconut Key Lime', 25000, 'minuman6.jpg', 'When youre in the mood for tropical drink, Its a fun twist on the coconut drink, featuring coconut milk and key lime juice', '-', NULL, NULL, NULL),
(15, 'Classic Mai Tai', 30000, 'minuman7.jpg', 'Fun and tasty tropical cocktail that everyone loves! The sweet but strong flavors of this drink go perfectly, yummy!', '-', NULL, NULL, NULL),
(16, 'Dulce Milshake', 35000, 'minuman8.jpg', 'Is a intensely delicious, creamy, and thick milkshake filled with flavor and goodness of caramel, all in one glass', '-', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2019_08_19_000000_create_failed_jobs_table', 1),
(9, '2021_05_18_053212_create_menus_table', 1),
(10, '2021_05_22_021320_create_orders_table', 1),
(11, '2021_05_23_225032_create_reservs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('096079b2d4182f18239824d9f78a41f7170d6ac454af70474d0e5626c8204d333e535c75dd9f29d5', 5, 1, 'bequeenToken', '[]', 0, '2021-05-23 19:43:23', '2021-05-23 19:43:23', '2022-05-24 02:43:23'),
('0dcf136e79d9214ccdeb5f173dace005e56136ee13df44e6d83e0559d6dc7081444ea95c1909fafc', 3, 1, 'bequeenToken', '[]', 0, '2021-05-23 19:20:24', '2021-05-23 19:20:24', '2022-05-24 02:20:24'),
('1912de60481498e4654b5ed335b0fbe9a3747980f47545bbe899cbbd49c00fb462bcc0f5e7e2dfee', 3, 1, 'bequeenToken', '[]', 0, '2021-05-23 19:59:27', '2021-05-23 19:59:27', '2022-05-24 02:59:27'),
('244fe7e8519a081e805d352669056c5bcc6600332a15feb6bd7e9cebf1460bef91880834274b169f', 4, 1, 'bequeenToken', '[]', 0, '2021-05-23 19:22:11', '2021-05-23 19:22:11', '2022-05-24 02:22:11'),
('330788c68840d7e6421a740ac30e4c68ff47698a0a21233eb054876948b111fd170f1259b832e58d', 2, 1, 'bequeenToken', '[]', 0, '2021-05-23 18:29:23', '2021-05-23 18:29:23', '2022-05-24 01:29:23'),
('3e8449b661c493fdd61db3a307b5854e5a35092ae8e98185bbc7cbfe009b5daeb8710ad47633cc58', 5, 1, 'bequeenToken', '[]', 0, '2021-05-23 19:43:47', '2021-05-23 19:43:47', '2022-05-24 02:43:47'),
('3f0b66880a30aa106babcdc37389d64b3cf8df67e9f9b8ec28010c2a032dd893bb15b9a56ab9b24d', 2, 1, 'bequeenToken', '[]', 0, '2021-05-23 19:13:21', '2021-05-23 19:13:21', '2022-05-24 02:13:21'),
('4f921e1e40378f95e2a2c3fb6abd4705237cc3310c8e7741c126e3cf9f7c291b36e47e6b2f1794f4', 3, 1, 'bequeenToken', '[]', 0, '2021-05-23 19:54:14', '2021-05-23 19:54:14', '2022-05-24 02:54:14'),
('60aab59aa70723dc36d307feed0537366d0800c8813f7af5add1a97694a7e548d05efc1724c36941', 5, 1, 'bequeenToken', '[]', 0, '2021-05-23 19:46:28', '2021-05-23 19:46:28', '2022-05-24 02:46:28'),
('7cb62646d8f5a82cdc7564ef5ce75ff04ec644f3e49ce263a3f294f27ce4aa29a5718a0ec59247e3', 2, 1, 'bequeenToken', '[]', 0, '2021-05-23 19:19:06', '2021-05-23 19:19:06', '2022-05-24 02:19:06'),
('da65614813d0e24c9da85d6f8be1b86e0e482ce9d71749a593e9597198ef3200949ecab3ed678a2f', 4, 1, 'bequeenToken', '[]', 0, '2021-05-23 19:25:21', '2021-05-23 19:25:21', '2022-05-24 02:25:21'),
('f22c4232d87c1617e0b5726125d41e7df47776c0a4694c5ec78e2a9873b908e92ab3d88e2d7bd0e0', 1, 1, 'bequeenToken', '[]', 0, '2021-05-23 19:47:23', '2021-05-23 19:47:23', '2022-05-24 02:47:23');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'IO1iRRmIFRDSjtCqZr1nbIYf57RGf6bIjSwlLHLP', NULL, 'http://localhost', 1, 0, 0, '2021-05-23 18:29:06', '2021-05-23 18:29:06'),
(2, NULL, 'Laravel Password Grant Client', 'uRybpxDDFyu1mubp4IYZDbb1LQTkP3Vitzx8T8GX', 'users', 'http://localhost', 0, 1, 0, '2021-05-23 18:29:06', '2021-05-23 18:29:06');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2021-05-23 18:29:06', '2021-05-23 18:29:06');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `price` double NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `reserv_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `menu_id`, `quantity`, `price`, `user_id`, `reserv_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 4, 2, 130000, 2, 1, '2021-05-23 18:29:49', '2021-05-23 18:29:49', NULL),
(2, 6, 2, 110000, 4, 3, '2021-05-23 19:26:29', '2021-05-23 19:26:29', NULL),
(3, 12, 1, 35000, 3, 2, '2021-05-23 19:26:29', '2021-05-23 19:26:29', NULL),
(4, 7, 1, 60000, 3, 2, '2021-05-23 19:32:36', '2021-05-23 19:32:36', NULL),
(5, 16, 1, 35000, 3, 2, '2021-05-23 19:36:44', '2021-05-23 19:36:44', NULL),
(6, 2, 7, 385000, 5, 4, '2021-05-23 19:45:00', '2021-05-23 19:45:00', NULL),
(7, 15, 2, 60000, 5, 4, '2021-05-23 19:45:18', '2021-05-23 19:45:18', NULL),
(8, 1, 1, 50000, 5, 4, '2021-05-23 19:46:39', '2021-05-23 19:46:39', NULL),
(9, 12, 1, 35000, 3, 2, '2021-05-23 19:54:20', '2021-05-23 19:54:20', NULL),
(10, 6, 1, 55000, 3, 2, '2021-05-23 19:59:48', '2021-05-23 19:59:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reservs`
--

CREATE TABLE `reservs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `PhoneNum` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `GuestNum` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `status` int(11) NOT NULL DEFAULT 0,
  `total_price` double NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reservs`
--

INSERT INTO `reservs` (`id`, `user_id`, `PhoneNum`, `GuestNum`, `date`, `time`, `note`, `status`, `total_price`, `created_at`, `updated_at`) VALUES
(1, 2, 'dasasd', '3', '12/12/1223', 'Lunch', 'asdads', 0, 130000, '2021-05-23 18:29:35', '2021-05-23 18:29:49'),
(2, 3, '087781', '2', '25/02/2012', 'Lunch', 'Minta di outdoor ya, terimakasi', 0, 220000, '2021-05-23 19:26:18', '2021-05-23 19:59:48'),
(3, 4, '1241412', '5', '12/12/2020', 'Lunch', 'outdoor view bagus', 0, 110000, '2021-05-23 19:26:19', '2021-05-23 19:26:29'),
(4, 5, '08123214124', '4', '12/12/2021', 'Lunch', 'outdoor', 0, 495000, '2021-05-23 19:44:42', '2021-05-23 19:46:39');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_admin` tinyint(1) NOT NULL DEFAULT 0,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `is_admin`, `password`, `status`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', 'admin@devtest.com', 1, '$2y$10$ew6UYeu1ouiiW4Vsor7q7.4NCS3TeXwe9CLPT.aPz.2oa0V.oUCce', 0, NULL, '2021-05-23 18:29:04', '2021-05-23 18:29:04', NULL),
(2, 'disappear', 'disappear@devtest.com', 0, '$2y$10$SZ1Gbak0tL6VENqVmiJsYuGsTNFAhtd8tVbG4Fm8hHrBvmmGwvWCe', 2, NULL, '2021-05-23 18:29:23', '2021-05-23 18:29:35', NULL),
(3, 'Safitri', 'saf@gmail.com', 0, '$2y$10$Bu0h2d4V6WU3qMv1W9/CwuYQT2yW2PBb.joKh.PrsKjyb4mAa4U6i', 2, NULL, '2021-05-23 19:20:24', '2021-05-23 19:26:18', NULL),
(4, 'chandra', 'chandra@gmail.com', 0, '$2y$10$Igy1w1/qZMpJ09BmpaamBehLI7Gf0uj2A7Lfijf9DD/2sR.auG3.m', 2, NULL, '2021-05-23 19:22:11', '2021-05-23 19:26:19', NULL),
(5, 'coba', 'coba@gmail.com', 0, '$2y$10$AEvhKif8mvT8hJ9DNFgSfO6Nhc4hyp1ndqzUBEKlGGJOU1pDfqpei', 2, NULL, '2021-05-23 19:43:23', '2021-05-23 19:44:42', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `reservs`
--
ALTER TABLE `reservs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `reservs`
--
ALTER TABLE `reservs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
